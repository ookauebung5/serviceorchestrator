package de.hbrs.serviceorchestrator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hbrs.wirschiffendas.data.entity.AlgorithmIdentifier;
import de.hbrs.wirschiffendas.data.entity.Configuration;
import de.hbrs.wirschiffendas.data.entity.Status;
import de.hbrs.wirschiffendas.data.entity.StatusTransferItem;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@CircuitBreaker(name = "liquidCheck")
@EnableAsync
public class AnalyseService {

    /*
        Services im Orchester:
            Liquid --> 8082
            Mechanic --> 8083
            Software --> 8084
     */

    private static final String LIQUID_SERVICE = "liquidCheck";

    @Async
    @PostMapping(value = "/analyse", consumes = "application/json")
    public void analyse(@RequestBody String json) {
        Configuration conf = null;
        try {
            conf = new ObjectMapper().readValue(json, Configuration.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        runMicroservices(conf, null);
    }

    @PostMapping(value = "/analyse/{id}", consumes = "application/json")
    public void analyseSpecific(@RequestBody String json, @PathVariable String id) {
        try {
            Configuration configuration = new ObjectMapper().readValue(json, Configuration.class);
            AlgorithmIdentifier algorithmIdentifier;
            switch (id.toLowerCase()) {
                case "liquid" -> algorithmIdentifier = AlgorithmIdentifier.LIQUID;
                case "software" -> algorithmIdentifier = AlgorithmIdentifier.SOFTWARE;
                case "mechanical" -> algorithmIdentifier = AlgorithmIdentifier.MECHANICAL;
                default -> throw new NullPointerException();
            }
            runMicroservices(configuration, algorithmIdentifier);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Async
    public void runMicroservices(Configuration configuration, @Nullable AlgorithmIdentifier identifier) {
        if (identifier == null) {
            callLiquidCheck(configuration);
            callMechanicalCheck(configuration);
            callSoftwareCheck(configuration);
        } else {
            switch (identifier) {
                case LIQUID -> callLiquidCheck(configuration);
                case SOFTWARE -> callSoftwareCheck(configuration);
                case MECHANICAL -> callMechanicalCheck(configuration);
            }
        }
    }

    @Async
    void callSoftwareCheck(Configuration configuration) {
        setRunning(AlgorithmIdentifier.SOFTWARE);
        String url = "http://localhost:8084/checkConfig";
        RestTemplate restTemplate = new RestTemplate();

        try {
            URI uri = new URI(url);
            ResponseEntity<Configuration> responseEntity = restTemplate.postForEntity(uri, configuration, Configuration.class);
            System.out.println(responseEntity.getStatusCode());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Async
    void callMechanicalCheck(Configuration configuration) {
        setRunning(AlgorithmIdentifier.MECHANICAL);
        String url = "http://localhost:8083/checkConfig";
        RestTemplate restTemplate = new RestTemplate();

        try {
            URI uri = new URI(url);
            ResponseEntity<Configuration> responseEntity = restTemplate.postForEntity(uri, configuration, Configuration.class);
            System.out.println(responseEntity.getStatusCode());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Retry(name = LIQUID_SERVICE, fallbackMethod = "liquidFallback")
    @Async
    public void callLiquidCheck(Configuration config) {
        setRunning(AlgorithmIdentifier.LIQUID);
        String url = "http://localhost:8082/checkConfig";
        RestTemplate restTemplate = new RestTemplate();

        try {
            URI uri = new URI(url);
            ResponseEntity<Configuration> responseEntity = restTemplate.postForEntity(uri, config, Configuration.class);
            System.out.println(responseEntity.getStatusCode());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Async
    public void setRunning(AlgorithmIdentifier algorithmIdentifier) {
        Status status = Status.RUNNING;
        StatusTransferItem statusTransferItem = new StatusTransferItem(algorithmIdentifier, status);
        String url = "http://localhost:8080/statusUpdate";

        RestTemplate restTemplate = new RestTemplate();
        try {
            URI uri = new URI(url);
            ResponseEntity<StatusTransferItem> responseEntity = restTemplate.postForEntity(uri, statusTransferItem, StatusTransferItem.class);
            System.out.println(responseEntity.getStatusCode());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void liquidFallback() {
        Status status = Status.FAILED;
        StatusTransferItem statusTransferItem = new StatusTransferItem(AlgorithmIdentifier.LIQUID, status);
        String url = "http://localhost:8080/statusUpdate";

        RestTemplate restTemplate = new RestTemplate();
        try {
            URI uri = new URI(url);
            ResponseEntity<StatusTransferItem> responseEntity = restTemplate.postForEntity(uri, statusTransferItem, StatusTransferItem.class);
            System.out.println(responseEntity.getStatusCode());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

}
